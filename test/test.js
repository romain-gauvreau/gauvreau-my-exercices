import { expect } from 'chai';
import { my_sum } from "../day-1/exercise-0.js";
import { my_display_alpha } from "../day-1/exercise-1.js";
import { my_display_alpha_reverse } from "../day-1/exercise-2.js";
import { my_alpha_number } from "../day-1/exercise-3.js";
import { my_size_alpha } from '../day-1/exercise-4.js';
import { my_array_alpha } from '../day-1/exercise-5.js';
import { my_length_array } from '../day-1/exercise-6.js';
import { my_is_posi_neg } from '../day-1/exercise-7.js';


describe("exercicse-0", function () {
  describe("my_sum", function () {
    it("2+2 should equals 4", function () {
      var basicSum = my_sum(2, 2);
      expect(basicSum).to.equal(4);
    });
    it("2+'0' should equals 0", function () {
      var basicSum = my_sum(2, '0');
      expect(basicSum).to.equal(0);
    });
    it("2+undefined should equals 0", function () {
      var basicSum = my_sum(2, undefined);
      expect(basicSum).to.equal(0);
    });
  });
});

describe("exercicse-1", function () {
  describe("my_display_alpha", function () {
    it("should display the alphabet", function () {
      var alphabet = my_display_alpha();
      expect(alphabet).to.equal('abcdefghijklmnopqrstuvwxyz');
    });
  });
});

describe("exercicse-2", function () {
  describe("my_display_alpha_reverse", function () {
    it("should display the alphabet reversed", function () {
      var alphabet = my_display_alpha_reverse();
      expect(alphabet).to.equal('zyxwvutsrqponmlkjihgfedcba');
    });
  });
});


describe("exercicse-3", function () {
  describe("my_alpha_number", function () {
    it("should 2 equals '2'", function () {
      var two = my_alpha_number(2);
      expect(two).to.equal('2');
    });
  });
});

describe("exercicse-4", function () {
  describe("my_size_alpha", function () {
    it("'abc' should return 3", function () {
      var length = my_size_alpha('abc');
      expect(length).to.equal(3);
    });
  });
});

describe("exercicse-5", function () {
  describe("my_array_alpha", function () {
    it("'abc' should return ['a','b','c']", function () {
      var array = my_array_alpha('abc');
      expect(array).to.eql(['a', 'b', 'c']);
    });
  });
});

describe("exercicse-6", function () {
  describe("my_length_array", function () {
    it(" ['a','b','c'] should return 3", function () {
      var arrayLength = my_length_array(['a', 'b', 'c']);
      expect(arrayLength).to.equal(3);
    });
    it(" [] should return 0", function () {
      var arrayLength = my_length_array([]);
      expect(arrayLength).to.equal(0);
    });
  });
});

describe("exercicse-7", function () {
  describe("my_is_posi_neg", function () {
    it("3 should return 'POSITIF'", function () {
      var result = my_is_posi_neg(3);
      expect(result).to.equal("POSITIF");
    });
    it("-3 should return 'NEGATIF'", function () {
      var result = my_is_posi_neg(-3);
      expect(result).to.equal("NEGATIF");
    });
    it("undefined should return 'POSITIF'", function () {
      var result = my_is_posi_neg(undefined);
      expect(result).to.equal("POSITIF");
    });
  });
});