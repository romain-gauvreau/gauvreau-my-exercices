export const my_length_array = (array) => {
    var length = 0;
    while (array[length] !== undefined)
        length++;
    return length;
};