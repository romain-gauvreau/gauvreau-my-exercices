export const my_sum = (a, b) => {

    if (a === undefined) {
        return 0;
    }

    if (b === undefined) {
        return 0;
    }

    if (a == null) {
        return 0;
    }

    if (b == null) {
        return 0;
    }

    if (typeof a !== "number") {
        return 0;
    }

    if (typeof b !== "number") {
        return 0;
    }

    return a + b;
};
