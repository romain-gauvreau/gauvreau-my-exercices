import { my_display_alpha } from "./exercise-1.js";

export const my_display_alpha_reverse = () => {
    const alphabet = my_display_alpha();
    return alphabet.split("").reverse().join("");
};