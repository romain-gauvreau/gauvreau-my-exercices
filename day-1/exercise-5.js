import { my_size_alpha } from "./exercise-4.js";

export const my_array_alpha = (str) => {
    let array = [];
    for (let i = 0; i < my_size_alpha(str); i++) {
        array[i] = str[i];
    }
    return array;
};