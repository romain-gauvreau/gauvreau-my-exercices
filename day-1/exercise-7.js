export const my_is_posi_neg = (a) => {
    if (typeof a === 'number' && a < 0) {
        return 'NEGATIF';
    } else if (a === 0) {
        return 'NEUTRAL'
    }
    return 'POSITIF';
};

